Pour ce projet (non terminé), j'ai commencé par créer un Use case.

J'ai choisi de ne pas prendre en compte la création de comptes utilisateurs avec différents rôles, ici le budget est partagé entre toute la famille. En arrivant sur l'appli, l'utilisateur voit le solde de son budget ainsi que les différentes opérations. Il peut en ajouter, les éditer, ou les supprimer. Il peut également les "geler", ce qui empêche par la suite de les modifier et de les supprimer. Il a aussi la possibilité de trier les opérations par catégories, ainsi que de voir diverses statistiques, comme l'évolution des dépenses châque mois.

J'ai par la suite créé un diagramme de classe qui comprend une classe opération, et une classe catégorie, avec une relation Many to One. 